#include "my_math.h"
#include "stdio.h"
#include <iostream>

using namespace std;


/** @brief Example application

    This is an example how the addition, multiplication, and subtraction defined
    in the my_math library can be used in a short program.

    @author H. Anzt, KIT
    @date March 2019
    */
int main() {
    cout << "* Addition:" << endl;
    cout << " If I add 5 and 8, I get " << add_numbers(5,8) << endl;
    cout << "  " << endl;
    cout << "* Subtraction:" << endl;
    cout << " If I subtract 5 from 8, I get " << subtract_numbers(8,5) << endl;
    cout << "  " << endl;
    cout << "* Multiplication:" << endl;
    cout << " If I multiply 5 and 8, I get " << multiply_numbers(5,8) << endl;
    cout << "  " << endl;

    return 0;
}
