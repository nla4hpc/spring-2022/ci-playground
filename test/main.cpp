
#include "gtest/gtest.h"


/** @brief Addition tester

    This unit_tester calls all unit tests defined.
    The original code is from: https://github.com/pothitos/gtest-demo-gitlab.git
    
    @author H. Anzt, KIT
    @date March 2019
    */
int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
